package controller

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"sort"

	"gitlab.com/kappelmans/models-kreva/portfolio/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func GetPortfolio(col *mgo.Collection, key string) model.Portfolio {
	results := []model.Portfolio{}

	col.FindId(bson.ObjectIdHex(key)).All(&results)

	return results[0]
}

func GetPortfolioList(col *mgo.Collection, dataOwnerId string) []model.Portfolio {
	results := []model.Portfolio{}

	col.Find(bson.M{"dataowner": bson.M{"$eq": dataOwnerId}}).All(&results)

	return results
}

func CreatePortfolio(col *mgo.Collection, dataownerId string, jsonCreateBarray []byte) ([]byte, error) {

	//Save data into Job struct
	var _portfolio model.Portfolio
	err := json.Unmarshal(jsonCreateBarray, &_portfolio)
	if err != nil {
		fmt.Println("Err2", err.Error())
		//			http.Error(w, err.Error(), 500)
		return nil, err
	}

	_portfolio.DataOwnerID = dataownerId

	//Insert job into MongoDB
	if _portfolio.ID != nil {
		err = col.UpdateId(_portfolio.ID, &_portfolio)
	} else {
		err = col.Insert(&_portfolio)
	}

	//Convert job struct into json
	jsonString, err := json.Marshal(_portfolio)
	if err != nil {
		fmt.Println("Err1", err.Error())
		//		http.Error(w, err.Error(), 500)
		return nil, err
	}
	fmt.Println("OK!")
	return jsonString, nil

}

func DeletePortfolio(col *mgo.Collection, key string) error {
	return col.RemoveId(bson.ObjectIdHex(key))
}

func DeletePortfolioCashFlow(col *mgo.Collection, portfolioKey string, cashflowKey string) error {
	portfolio := GetPortfolio(col, portfolioKey)
	portfolio.DeleteCashFlow(bson.ObjectIdHex(cashflowKey))
	col.UpdateId(portfolio.ID, portfolio)
	return nil
}

func CreatePortfolioHistoryData(portcol *mgo.Collection, jsonHistoryData []byte, portfolioKey string) error {

	var historyData *model.HistroyData

	err := json.Unmarshal(jsonHistoryData, &historyData)
	if err != nil {
		fmt.Println("Err2", err.Error())
		return err
	}

	historyData.ID = bson.NewObjectId()

	portfolio := GetPortfolio(portcol, portfolioKey)
	portfolio.AddHistoryData(historyData)
	portcol.UpdateId(portfolio.ID, portfolio)

	fmt.Println(portfolio)

	return nil

}

func CreatePortfolioCashFlow(portcol *mgo.Collection, jsonCashFlow []byte, portfolioKey string) error {

	var cashFlow *model.CashFlow

	err := json.Unmarshal(jsonCashFlow, &cashFlow)
	if err != nil {
		fmt.Println("Err2", err.Error())
		return err
	}

	cashFlow.ID = bson.NewObjectId()

	portfolio := GetPortfolio(portcol, portfolioKey)
	portfolio.AddCashFlow(cashFlow)
	portcol.UpdateId(portfolio.ID, portfolio)

	fmt.Println(portfolio)

	return nil

}

func CreatePortfolioPosition(portcol *mgo.Collection, jsonPosition []byte, portfolioKey string) error {

	var position *model.Position

	err := json.Unmarshal(jsonPosition, &position)
	if err != nil {
		fmt.Println("Err2", err.Error())
		//			http.Error(w, err.Error(), 500)
		return err
	}

	positionKey := bson.NewObjectId()
	position.ID = positionKey

	portfolio := GetPortfolio(portcol, portfolioKey)

	//portfolio.PositionList = append(portfolio.PositionList, *position)
	portfolio.UpdatePosition(*position)
	portcol.UpdateId(portfolio.ID, portfolio)

	fmt.Println(portfolio)

	return nil
}

func GetPortfolioDetail(col *mgo.Collection, portfolioKey string) model.Portfolio {
	portfolio := GetPortfolio(col, portfolioKey)
	portfolio.PositionList, portfolio.Summary = GetPortfolioPositionList(col, portfolioKey)

	portfolio.Summary.NAV = math.Round(portfolio.Summary.NAV*100) / 100

	var latest, currentYear model.HistroyData
	latest.Time = 1900

	//sort records
	sort.Slice(portfolio.HistroyDataList, func(i, j int) bool { return portfolio.HistroyDataList[i].Time < portfolio.HistroyDataList[j].Time })
	for i, historyRecord := range portfolio.HistroyDataList {
		portfolio.HistroyDataList[i].Change = math.Round((historyRecord.NAV-latest.NAV)*100) / 100
		latest = historyRecord
	}

	var cashFlowTotal model.CashFlow
	cashFlowTotal.Type = 1

	sort.Slice(portfolio.CashFlowList, func(i, j int) bool { return portfolio.CashFlowList[i].Time > portfolio.CashFlowList[j].Time })

	for _, cf := range portfolio.CashFlowList {
		cashFlowTotal.EUR += cf.EUR
		cashFlowTotal.USD += cf.USD
	}

	portfolio.CashFlowList = append(portfolio.CashFlowList, cashFlowTotal)
	currentYear.Time = 2020

	if len(portfolio.HistroyDataList) > 0 {
		if len(portfolio.CashFlowList) > 0 && portfolio.CashFlowList[0].Time == currentYear.Time {
			currentYear.Return = math.Round((portfolio.Summary.NAV/(latest.NAV+portfolio.CashFlowList[0].USD)-1)*10000) / 100
			currentYear.Change = math.Round((portfolio.Summary.NAV-latest.NAV-portfolio.CashFlowList[0].USD)*100) / 100
		} else {
			currentYear.Return = math.Round((portfolio.Summary.NAV/latest.NAV-1)*10000) / 100
			currentYear.Change = math.Round((portfolio.Summary.NAV-latest.NAV)*100) / 100
		}

	} else if len(portfolio.CashFlowList) > 1 {
		currentYear.Return = math.Round((portfolio.Summary.NAV/cashFlowTotal.USD-1)*10000) / 100
		currentYear.Change = math.Round((portfolio.Summary.NAV-cashFlowTotal.USD)*100) / 100
	}
	currentYear.NAV = portfolio.Summary.NAV
	currentYear.Time = 2020

	portfolio.HistroyDataList = append(portfolio.HistroyDataList, currentYear)

	return portfolio
}

func GetPortfolioPositionList(col *mgo.Collection, portfolioKey string) ([]model.Position, model.HistroyData) {
	portfolio := GetPortfolio(col, portfolioKey)

	c := make(chan model.Position)

	for i, _ := range portfolio.PositionList {
		if portfolio.PositionList[i].Type == 0 {
			go updatePosition(&portfolio.PositionList[i], c)
		}
	}

	var updatePositionList []model.Position

	for i, _ := range portfolio.PositionList {
		if portfolio.PositionList[i].Type == 0 {
			updatePositionList = append(updatePositionList, <-c)
		}
	}

	var summary model.HistroyData

	for i, _ := range portfolio.PositionList {
		if portfolio.PositionList[i].Type == 1 {
			updatePositionList = append(updatePositionList, portfolio.PositionList[i])
			summary.NAV += portfolio.PositionList[i].Quantity
		} else {
			summary.NAV += portfolio.PositionList[i].Detail.Value
			summary.Change += portfolio.PositionList[i].Detail.Change
		}
	}

	summary.Return = summary.Change / (summary.NAV - summary.Change)

	fmt.Println(updatePositionList)
	return updatePositionList, summary
}

func GetPortfolioCashFlowList(col *mgo.Collection, portfolioKey string) []model.CashFlow {
	portfolio := GetPortfolio(col, portfolioKey)

	return portfolio.CashFlowList
}

func GetPortfolioHistroyDataList(col *mgo.Collection, portfolioKey string) []model.HistroyData {
	portfolio := GetPortfolio(col, portfolioKey)

	return portfolio.HistroyDataList
}

func updatePosition(p *model.Position, c chan model.Position) {
	url := fmt.Sprintf("https://cloud.iexapis.com/beta/stock/%s/quote?token=pk_e1403dc65d2040509778afd4fbfe1888", p.Ticker)
	resp, _ := http.Get(url)
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	var quoteData *model.QuoteData
	err := json.Unmarshal(body, &quoteData)
	if err != nil {
		fmt.Println("Err2", err.Error())
		return
	}

	//set position detail
	p.Detail.Value = quoteData.LatestPrice * p.Quantity
	p.Detail.Change = quoteData.Change * p.Quantity

	p.Quote = *quoteData
	fmt.Println(p)
	c <- *p

}
