package model

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

type QuoteData struct {
	Ticker      string `json:"symbol"`
	CompanyName string `json:"companyName"`

	Open        float64 `json:"open"`
	High        float64 `json:"high"`
	Low         float64 `json:"low"`
	Close       float64 `json:"close"`
	LatestPrice float64 `json:"latestPrice"`

	Change        float64 `json:"change"`
	ChangePercent float64 `json:"changePercent"`
	YtdChange     float64 `json:"ytdChange"`
}

type PositionDetail struct {
	Value  float64 `json:"value"`
	Change float64 `json:"change"`
}

type Position struct {
	ID       bson.ObjectId  `bson:"_id,omitempty" json:"_id,omitempty"`
	Type     int            `json:"type"`
	Ticker   string         `json:"ticker"`
	Quantity float64        `json:"quantity"`
	Quote    QuoteData      `json:"quote"`
	Detail   PositionDetail `json:"detail"`
}

type CashFlow struct {
	ID   bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Type int           `json:"type"`
	Time int           `json:"time"`
	EUR  float64       `json:"eur"`
	USD  float64       `json:"usd"`
}

type Transaction struct {
	TradeDate   time.Time `json:tradedate`
	Action      string    `json: action`
	Symbol      string    `json: symbol`
	Description string    `json: description`
	Quantity    int       `json: qauntity`
	Price       float64   `json prive`
	FeeAndComm  float64   `json feesandcomm`
	Amount      float64   `json amount`
}

type HistroyData struct {
	ID     bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Time   int           `json:"time"`
	NAV    float64       `json:"nav"`
	Change float64       `json:"change"`
	Return float64       `json:"return"`
}

type Portfolio struct {
	ID              *bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	DataOwnerID     string         `bson:"dataowner" json:"dataowner"`
	Name            string         `json:"name"`
	PositionList    []Position     `bson:"positionlist,omitempty" json:"positionlist"`
	CashFlowList    []CashFlow     `bson:"cashflowlist" json:"cashflowlist"`
	HistroyDataList []HistroyData  `bson:"historydatalist" json:"historydatalist"`
	Summary         HistroyData    `json:"summary"`
}

func (p *Portfolio) UpdatePosition(position Position) {
	portfPositionIndex, portfPosition := p.GetPosition(position.Ticker)
	if portfPosition != nil {
		if position.Quantity > 0 {
			p.PositionList[portfPositionIndex].Quantity = position.Quantity
		} else {
			p.PositionList[portfPositionIndex] = p.PositionList[len(p.PositionList)-1]
			p.PositionList = p.PositionList[:len(p.PositionList)-1]
		}
	} else {
		p.PositionList = append(p.PositionList, position)
	}
}

func (p *Portfolio) GetPosition(ticker string) (int, *Position) {
	for i, position := range p.PositionList {
		if ticker == position.Ticker {
			return i, &position
		}
	}
	return -1, nil
}

func (p *Portfolio) AddCashFlow(cashFlow *CashFlow) {
	p.CashFlowList = append(p.CashFlowList, *cashFlow)
}

func (p *Portfolio) AddHistoryData(histroyData *HistroyData) {
	p.HistroyDataList = append(p.HistroyDataList, *histroyData)
}

func (p *Portfolio) DeleteCashFlow(key bson.ObjectId) {

	for i, cashFlow := range p.CashFlowList {
		fmt.Println("cf: ", cashFlow.ID, " - ", key, " - ", key == cashFlow.ID)
		if key == cashFlow.ID {
			p.CashFlowList[i] = p.CashFlowList[len(p.CashFlowList)-1]
			p.CashFlowList = p.CashFlowList[:len(p.CashFlowList)-1]
			break
		}
	}

	//    p.CashFlowList = append(p.CashFlowList, cashFlow)
}

/*
      let positionList = [
          { ticker: 'AAPL', quantity: 577.0087 },
          { ticker: 'GOOG', quantity: 112 },
          { ticker: 'GOOGL', quantity: 35 },
          { ticker: 'GLW', quantity: 875.0683 },
          { ticker: 'APTV', quantity: 200 },
          { ticker: 'DLPH', quantity: 66 },
          { ticker: 'GILD', quantity: 600 },
          { ticker: 'BRK.A', quantity: 3 },
          { ticker: 'BRK.B', quantity: 100+1035 },
          { ticker: 'EBAY', quantity: 375 },
          { ticker: 'TSM', quantity: 521 },
          { ticker: 'MA', quantity: 55.3237 },
          { ticker: 'MSFT', quantity: 17.271 },

          { ticker: 'PYPL', quantity: 375 },
          { ticker: 'QQQ', quantity: 2100 } //QQQ-FP
      ]

      let cashFlowList = [
          { time: '2004', eur: 544550.79, usd: 544550.79 },
          { time: '2010', eur: 252120.88, usd: 252120.88 },
          { time: '2011', eur: 20828.6, usd: 20828.6 },
          { time: '2013', eur: 99958.76, usd: 99958.76 },
          { time: '2019', eur: 0, usd: 0 },
      ]

      let historyData = [
          {time:'2004',nav: 673274.52, return: 23.64},
          {time:'2005',nav:707490.72,return:5.08},
          {time:'2006',nav:750928.26,return:6.14},
          {time:'2007',nav:1042176.43,return:38.79},
          {time:'2008',nav:849285.49,return:-19.71},
          {time:'2009',nav:941182.32,return:10.82},
          {time:'2010',nav:1358521.55,return:13.29},
          {time:'2011',nav:1274942.21,return:-7.57},
          {time:'2012',nav:1339241.3,return:5.04},
          {time:'2013',nav:1561227.55,return:8.48},
          {time:'2014',nav:1765369.31,return:13.08},
          {time:'2015',nav:1789303.39,return:-4.58},
          {time:'2016',nav:2030908.02,return:13.50},
          {time:'2017',nav:2387263.72,return:17.55},
          {time:'2018',nav:2392767.19,return:0.23}

   ]
*/
