package requesthandler

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	genericrequesthandler "gitlab.com/kappelmans/go-lang-backend/requesthandler"
	"gitlab.com/kappelmans/models-kreva/portfolio/controller"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//TODO MAKE IT GENERIC!
func getCollection(db *mgo.Database, collectionName string) *mgo.Collection {
	return db.C(collectionName)
}

func PortfolioListGetHandler(db *mgo.Database, c *gin.Context) {
	//launch the querry
	datownerId := c.GetHeader("datownerId")
	c.JSON(http.StatusOK, controller.GetPortfolioList(getCollection(db, "portfolio"), datownerId))

}

func PortfolioDeleteHandler(db *mgo.Database, c *gin.Context) {
	userKey := c.Param("key")
	//err :=
	getCollection(db, "portfolio").RemoveId(bson.ObjectIdHex(userKey))
	c.JSON(http.StatusOK, "Remove OK: ")
}

func PortfolioPostHandler(db *mgo.Database, c *gin.Context) {

	//Retrieve body from http request
	b, err := c.GetRawData()
	datownerId := c.GetHeader("DatownerId")

	if err != nil {
		panic(err)
	}

	jsonString, _ := controller.CreatePortfolio(getCollection(db, "portfolio"), datownerId, b)

	c.JSON(http.StatusOK, jsonString)
}

func CreatePortfolioHistroyDataHandler(db *mgo.Database, c *gin.Context) {
	portfolioKey := c.Param("portfolio")
	b, err := c.GetRawData()

	controller.CreatePortfolioHistoryData(getCollection(db, "portfolio"), b, portfolioKey)

	if err != nil {
		panic(err)
	}
	c.JSON(http.StatusOK, "OK")
}

func CreatePortfolioCashFlowHandler(db *mgo.Database, c *gin.Context) {
	portfolioKey := c.Param("portfolio")
	b, err := c.GetRawData()

	controller.CreatePortfolioCashFlow(getCollection(db, "portfolio"), b, portfolioKey)

	if err != nil {
		panic(err)
	}
	c.JSON(http.StatusOK, "OK")
}

func DeletePortfolioCashFlowHandler(db *mgo.Database, c *gin.Context) {
	portfolioKey := c.Param("portfolio")
	cashflowKey := c.Param("cashflow")

	controller.DeletePortfolioCashFlow(getCollection(db, "portfolio"), portfolioKey, cashflowKey)

	c.JSON(http.StatusOK, "OK")
}

func SetPortfolioPositionHandler(db *mgo.Database, c *gin.Context) {
	portfolioKey := c.Param("portfolio")
	b, err := c.GetRawData()

	controller.CreatePortfolioPosition(getCollection(db, "portfolio"), b, portfolioKey)

	if err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, "OK")
}

func PortfolioPositionListGetHandler(db *mgo.Database, c *gin.Context) {
	portfolioKey := c.Param("portfolio")

	list, _ := controller.GetPortfolioPositionList(getCollection(db, "portfolio"), portfolioKey)
	fmt.Println(list)
	c.JSON(http.StatusOK, list)

}

func GetPortfolioDetailHandler(db *mgo.Database, c *gin.Context) {
	portfolioKey := c.Param("portfolio")

	list := controller.GetPortfolioDetail(getCollection(db, "portfolio"), portfolioKey)
	fmt.Println(list)
	c.JSON(http.StatusOK, list)

}

func PortfolioCashFlowListGetHandler(db *mgo.Database, c *gin.Context) {
	portfolioKey := c.Param("portfolio")

	list := controller.GetPortfolioCashFlowList(getCollection(db, "portfolio"), portfolioKey)
	fmt.Println(list)
	c.JSON(http.StatusOK, list)

}

func PortfolioHistroyDataListGetHandler(db *mgo.Database, c *gin.Context) {
	portfolioKey := c.Param("portfolio")

	list := controller.GetPortfolioHistroyDataList(getCollection(db, "portfolio"), portfolioKey)
	fmt.Println(list)
	c.JSON(http.StatusOK, list)

}

func GetRoutes(basePath string) []genericrequesthandler.Route {

	return []genericrequesthandler.Route{
		genericrequesthandler.Route{
			Action:        genericrequesthandler.GET,
			Path:          basePath + "/list",
			ReqestHandler: PortfolioListGetHandler,
		},
		genericrequesthandler.Route{
				Action:        genericrequesthandler.GET,
				Path:          basePath + "/detail/:portfolio",
				ReqestHandler: GetPortfolioDetailHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.POST,
			Path:          basePath + "/create",
			ReqestHandler: PortfolioPostHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.DELETE,
			Path:          basePath + "/delete/:key",
			ReqestHandler: PortfolioDeleteHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.POST,
			Path:          basePath + "/setposition/:portfolio",
			ReqestHandler: SetPortfolioPositionHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.POST,
			Path:          basePath + "/addcashflow/:portfolio",
			ReqestHandler: CreatePortfolioCashFlowHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.DELETE,
			Path:          basePath + "/deletecashflow/:portfolio/:cashflow",
			ReqestHandler: DeletePortfolioCashFlowHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.GET,
			Path:          basePath + "/positionlist/:portfolio",
			ReqestHandler: PortfolioPositionListGetHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.GET,
			Path:          basePath + "/cashflowlist/:portfolio",
			ReqestHandler: PortfolioCashFlowListGetHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.POST,
			Path:          basePath + "/addhistorydata/:portfolio",
			ReqestHandler: CreatePortfolioHistroyDataHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.GET,
			Path:          basePath + "/historydatalist/:portfolio",
			ReqestHandler: PortfolioHistroyDataListGetHandler,
		},
	}
}
