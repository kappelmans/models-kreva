package controller

import (
	"encoding/json"
	"fmt"

	"gitlab.com/kappelmans/models-kreva/realestate/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func GetGenericEntityDetail(col *mgo.Collection, key string) interface{} {
	results := []model.Property{}
	col.FindId(bson.ObjectIdHex(key)).All(&results)
	return results[0]
}

func GetGenericList(col *mgo.Collection, dataOwnerId string, result []interface{}) interface{} {
	fmt.Println("finding all entities with owner: " + dataOwnerId)
	col.Find(bson.M{"dataowner": bson.M{"$eq": dataOwnerId}}).All(&result)
	return result
}

func CreateEntityRentalAgreement(col *mgo.Collection, dataownerId string, jsonBinary []byte) ([]byte, error) {

	var rentalagreement model.RentalAgreement

	err := json.Unmarshal(jsonBinary, &rentalagreement)
	if err != nil {
		fmt.Println("Err2", err.Error())
		//			http.Error(w, err.Error(), 500)
		return nil, err
	}

	rentalagreement.DataOwnerID = dataownerId

	//Insert job into MongoDB
	if rentalagreement.ID != nil {
		err = col.UpdateId(rentalagreement.ID, &rentalagreement)
	} else {
		err = col.Insert(&rentalagreement)
	}

	//Convert job struct into json
	jsonString, err := json.Marshal(rentalagreement)
	if err != nil {
		fmt.Println("Err1", err.Error())
		//		http.Error(w, err.Error(), 500)
		return nil, err
	}
	fmt.Println(string(jsonString))
	return jsonString, nil
}

func CreateEntityProperty(col *mgo.Collection, datownerId string, jsonBinary []byte) ([]byte, error) {

	var property model.Property

	err := json.Unmarshal(jsonBinary, &property)
	if err != nil {
		fmt.Println("Err2", err.Error())
		//			http.Error(w, err.Error(), 500)
	}
	property.DataOwnerID = datownerId
	//Insert job into MongoDB
	if property.ID != nil {
		err = col.UpdateId(property.ID, &property)
	} else {
		err = col.Insert(&property)
	}

	if err != nil {
		panic(err)
	}

	//Convert job struct into json
	jsonString, err := json.Marshal(&property)
	if err != nil {
		fmt.Println("Err1", err.Error())
		//		http.Error(w, err.Error(), 500)
		return nil, err
	}
	fmt.Println(string(jsonString))
	return jsonString, nil
}
