package controller

import (
	"fmt"
	"time"

	"gitlab.com/kappelmans/models-kreva/realestate/model"
)

func CalculateRentalAdjustment(ra *model.RentalAgreement, year int) string {
	var adjustment model.RentalAdjustment

	//find index at the time of siganture
	sigDate := ra.ContractSignatureDate
	contStart := ra.ContractStartDate
	var newIndex float64
	var idxM, idxY int
	var sigM, sigY int

	if sigDate.Month() == 1 {
		sigM = 12
		sigY = sigDate.Year() - 1
	} else {
		sigM = int(sigDate.Month() - 1)
		sigY = sigDate.Year()
	}

	if contStart.Month() == 1 {
		idxM = 12
		idxY = year - 1
	} else {
		idxM = int(contStart.Month() - 1)
		idxY = year
	}

	newIndex = model.IndexLib[idxY-1970].IndexValue[idxM-1]
	baseIndex := model.IndexLib[sigY-1970].IndexValue[sigM-1]

	newRent := ra.Rent / baseIndex * newIndex

	adjustment.IndexValue = newIndex
	adjustment.IndexM = idxM
	adjustment.IndexY = idxY
	duePaymentList := make([]model.DuePayment, 0)
	duePaymentList = append(duePaymentList, model.DuePayment{DueAmount: newRent})
	adjustment.DuePaymentList = &duePaymentList

	return fmt.Sprintf("Index of contract: %3.2f (%d/%d) Index year %d is:  %3.2f (%d/%d)\tNew rent: %4.2f", baseIndex, sigM, model.IndexLib[sigY-1970].Year, year, newIndex, idxM, idxY, newRent)
	//return adjustment

}

func CalculateRentalAdjustments(ra *model.RentalAgreement) []string {
	result := make([]string, 1)

	startDate := *ra.ContractStartDate
	result = append(result, fmt.Sprintf("Rent: %10.2fstart date: %02d/%02d/%04d", ra.Rent, startDate.Day(), startDate.Month(), startDate.Year()))

	nextYear := time.Date(startDate.Year()+1, startDate.Month(), 1, 0, 0, 0, 0, time.UTC)
	for i := 0; time.Since(nextYear).Hours() > 0; i++ {
		result = append(result, CalculateRentalAdjustment(ra, nextYear.Year()))
		nextYear = time.Date(nextYear.Year()+1, nextYear.Month(), 1, 0, 0, 0, 0, time.UTC)
	}
	fmt.Println(result)

	return result
}
