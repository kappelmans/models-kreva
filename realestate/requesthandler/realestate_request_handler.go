package requesthandler

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	genericrequesthandler "gitlab.com/kappelmans/go-lang-backend/requesthandler"
	"gitlab.com/kappelmans/models-kreva/realestate/controller"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//TODO MAKE IT GENERIC!
func getCollection(db *mgo.Database, collectionName string) *mgo.Collection {
	return db.C(collectionName)
}

func GenericEntityPostHandler(collection string) genericrequesthandler.RequestHandlerFunction {
	//launch the querry
	return func(db *mgo.Database, c *gin.Context) {

		datownerId := c.GetHeader("DatownerId")
		//fmt.Println("datownerId: " + datownerId)

		//Retrieve body from http request
		b, err := c.GetRawData()
		if err != nil {
			panic(err)
		}

		switch collection {
		case "rentalagreement":
			jsonString, _ := controller.CreateEntityRentalAgreement(getCollection(db, collection), datownerId, b)
			c.JSON(http.StatusOK, jsonString)
			break
		case "property":
			jsonString, _ := controller.CreateEntityProperty(getCollection(db, collection), datownerId, b)
			c.JSON(http.StatusOK, jsonString)
			break
		}

	}
}

func GenericListGetHandler(collection string) genericrequesthandler.RequestHandlerFunction {
	//launch the querry
	return func(db *mgo.Database, c *gin.Context) {
		datownerId := c.GetHeader("datownerId")
		fmt.Println("datownerId: " + datownerId)
		c.JSON(http.StatusOK, controller.GetGenericList(getCollection(db, collection), datownerId, make([]interface{}, 0)))
	}
}

func GenericDeleteHandler(collection string) genericrequesthandler.RequestHandlerFunction {
	//launch the querry
	return func(db *mgo.Database, c *gin.Context) {
		datownerId := c.GetHeader("datownerId")
		fmt.Println("datownerId: " + datownerId)
		userKey := c.Param("key")
		getCollection(db, collection).RemoveId(bson.ObjectIdHex(userKey))
		c.JSON(http.StatusOK, "Remove OK: ")
	}
}

func GenericEntityDetailHandler(collection string) genericrequesthandler.RequestHandlerFunction {
	//launch the querry
	return func(db *mgo.Database, c *gin.Context) {
		datownerId := c.GetHeader("datownerId")
		fmt.Println("datownerId: " + datownerId)

		userKey := c.Param("key")
		c.JSON(http.StatusOK, controller.GetGenericEntityDetail(getCollection(db, collection), userKey))
	}
}

func GetRoutes(basePath string) []genericrequesthandler.Route {
	//var _Entity interface{}

	return []genericrequesthandler.Route{
		genericrequesthandler.Route{
			Action:        genericrequesthandler.GET,
			Path:          basePath + "/property/list",
			ReqestHandler: GenericListGetHandler("property"),
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.GET,
			Path:          basePath + "/property/detail/:key",
			ReqestHandler: GenericEntityDetailHandler("property"),
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.POST,
			Path:          basePath + "/property/create",
			ReqestHandler: GenericEntityPostHandler("property"),
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.DELETE,
			Path:          basePath + "/property/delete/:key",
			ReqestHandler: GenericDeleteHandler("property"),
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.GET,
			Path:          basePath + "/rentalagreement/list",
			ReqestHandler: GenericListGetHandler("rentalagreement"),
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.GET,
			Path:          basePath + "/rentalagreement/detail/:key",
			ReqestHandler: GenericEntityDetailHandler("rentalagreement"),
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.POST,
			Path:          basePath + "/rentalagreement/create",
			ReqestHandler: GenericEntityPostHandler("rentalagreement"),
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.DELETE,
			Path:          basePath + "/rentalagreement/delete/:key",
			ReqestHandler: GenericDeleteHandler("rentalagreement"),
		},
	}
}
