package model

import (
	"encoding/json"
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

type Address struct {
	ID         bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Street     string        `json:"street"`
	PostalCode string        `json:"postalcode"`
	Place      string        `json:"place"`
}

type Property struct {
	ID               *bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	DataOwnerID      string         `bson:"dataowner" json:"dataowner"`
	Name             string         `json:"name"`
	IDOwner          bson.ObjectId  `bson:"ownerid,omitempty" json:"ownerid,omitempty"`
	Address          Address        `bson:"address" json:"address"`
	CadastralIncome  float64        `json:"cadastralincome,string"`
	HabitableSurface float64        `json:"habitablesurface,string"`
	TerrainSize      float64        `json:"terrainsize,string"`
}

type DuePayment struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	DueType     int           `json:"duetype,string"`
	DueAmount   float64       `json:"dueamount,string"`
	VatType     int           `json:"vattype,string"`
	Periodicity int           `json:"periodicity,string"`
}

type RentalAdjustment struct {
	ID             bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	AdjustmentDate *time.Time    `json:"adjustmentdate"`
	IndexValue     float64       `json:"indexvalue"`
	IndexM         int           `json:"indexm"`
	IndexY         int           `json:"indexy"`
	DuePaymentList *[]DuePayment `json:"duepaymentlist,string"`
}

type DuePaymentSchedule struct {
	ID             bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Active         bool          `json:"active,string"`
	AdjustmentDate *time.Time    `json:"adjustmentdate"`
	DuePaymentList *[]DuePayment `json:"duepaymentlist,string"`
}

type RentalAgreement struct {
	ID                    *bson.ObjectId   `bson:"_id,omitempty" json:"_id,omitempty"`
	DataOwnerID           string           `bson:"dataowner,omitempty" json:"dataowner,omitempty"`
	OwnRef                *string          `bson:"ownref,omitempty" json:"ownref,omitempty`
	IDProperty            bson.ObjectId    `bson:"propertyid,omitempty" json:"propertyid,omitempty"`
	IDLessor              bson.ObjectId    `bson:"lessorid,omitempty" json:"lessorid,omitempty"`
	LesseeIDList          *[]bson.ObjectId `bson:"lesseelist,omitempty" json:"lesseelist,omitempty"`
	CorrespondanceTitle   string           `json:"correspondancetitle"`
	Address               *Address         `bson:"correcpondanceaddress,omitempty" json:"correcpondanceaddress,omitempty"`
	Rent                  float64          `json:"rent,string"`
	BaseRent              float64          `json:"baserent,string"`
	ContractSignatureDate *time.Time       `bson:"contractsignaturedate,omitempty" json:"contractsignaturedate,omitempty"`
	ContractStartDate     *time.Time       `bson:"contractstartdate,omitempty" json:"contractstartdate,omitempty"`
	IndexMonth            int              `json:"indexmonth,string"`
	Index                 float64          `json:"index,string"`
	IndexType             int              `json:"indextype,string"`
	AdvancesOnWater       float64          `json:"advancesonwater,string"`
	AdvancesOnCharges     float64          `json:"advancesoncharges,string"`
}

type Entity interface {
	ReadFromJson(jsonBinary []byte) error
}

func (rentalagreement *RentalAgreement) ReadFromJson(jsonBinary []byte) error {
	err := json.Unmarshal(jsonBinary, rentalagreement)
	if err != nil {
		fmt.Println("Err2", err.Error())
		//			http.Error(w, err.Error(), 500)
		return err
	}
	return nil
}

func (property *Property) ReadFromJson(jsonBinary []byte) error {
	err := json.Unmarshal(jsonBinary, property)
	if err != nil {
		fmt.Println("Err2", err.Error())
		//			http.Error(w, err.Error(), 500)
		return err
	}
	return nil
}
