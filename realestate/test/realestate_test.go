package main

import (
	"gitlab.com/kappelmans/models-kreva/realestate/controller"
	"gitlab.com/kappelmans/models-kreva/realestate/model"
	"os"
	"testing"
	//	"encoding/json"

	"gitlab.com/kappelmans/go-lang-backend/approuter"
	"gitlab.com/kappelmans/go-lang-backend/config"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var rentalAgreementCollection *mgo.Collection
var db *mgo.Database
var dataowner = "Urro NV"
var test = 15

func TestCollectionCount(t *testing.T) {
	var result []model.RentalAgreement

	rentalAgreementCollection.Find(bson.M{"dataowner": bson.M{"$eq": dataowner}}).All(&result)

	//rentalAgreementCollection.Find(nil).All(&result)

	if len(result) == 0 {
		t.Errorf("We got %d rental aggreementes", len(result))
	} else {
		t.Logf("We got %d rental aggreementes for %s", len(result), dataowner)
	}

	for _, aggreement := range result {

		/*		var rentalAgreement model.RentalAgreement
				err := json.Unmarshal([]byte(aggreement), &rentalAgreement)
				t.Logf("Err: %s", err)
		*/
		res := controller.CalculateRentalAdjustments(&aggreement)
		for _, val := range res {
			t.Logf("%s", val)
		}

	}

}

func TestSum1(t *testing.T) {

	test := db.Session.Ping()
	if test != nil {
		t.Log("Ping to DB NOK")
	} else {
		t.Log("Ping to DB OK")
	}
}

func TestMain(m *testing.M) {
	// call flag.Parse() here if TestMain uses flags
	test = 10

	config := config.GetConfig()
	app := &approuter.AppRouter{}
	app.Initialize(config)
	rentalAgreementCollection = app.GetDB().C("rentalagreement")
	db = app.GetDB()

	os.Exit(m.Run())
}
